//
//  ItemModel.swift
//  bnetLayout01
//
//  Created by Denis Velikanov on 22.09.2021.
//

import Foundation

struct ItemModel: Codable {
    var title: String
    var test: Int
    var sub_themes: Int
    var blocks: Int
    var blocks_count: Int
    var status: String
}

