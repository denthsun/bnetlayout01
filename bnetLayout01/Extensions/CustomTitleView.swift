//
//  CustomTitleView.swift
//  bnetLayout01
//
//  Created by Denis Velikanov on 23.09.2021.
//

import Foundation
import UIKit

class CustomTitleView: UIView {
    
    var titleText: String = ""
    let button = CustomTitleButton()
    let selectedView = UIView()
    
    var buttonEnabled: Bool = true {
        didSet {
            selectedView.isHidden = buttonEnabled ? false : true
            button.buttonEnabled = buttonEnabled ? true : false
        }
    }
    
    init(frame: CGRect, isEnabled: Bool, titleText: String) {
        super.init(frame: frame)
        self.titleText = titleText
        self.buttonEnabled = isEnabled
        setupUI()
        constraintUI()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        [button, selectedView].forEach { self.addSubview($0) }
        
        button.setTitle("\(titleText)", for: .normal)
        button.setTitle("\(titleText)", for: .selected)
        
        selectedView.backgroundColor = .lightGray
        
        if buttonEnabled {
            button.buttonEnabled = true
            selectedView.isHidden = false
        } else {
            button.buttonEnabled = false
            selectedView.isHidden = true
        }
    }
    
    func constraintUI() {
        button.anchor(top: self.safeAreaLayoutGuide.topAnchor,
                      leading: self.safeAreaLayoutGuide.leadingAnchor,
                      bottom: self.safeAreaLayoutGuide.bottomAnchor,
                      trailing: self.safeAreaLayoutGuide.trailingAnchor,
                      padding: .init(top: 0, left: 5, bottom: 15, right: 5))
        selectedView.anchor(top: button.safeAreaLayoutGuide.bottomAnchor,
                            leading: self.safeAreaLayoutGuide.leadingAnchor,
                            bottom: self.safeAreaLayoutGuide.bottomAnchor,
                            trailing: self.safeAreaLayoutGuide.trailingAnchor)
        button.heightAnchor.constraint(equalToConstant: self.layer.bounds.height / 2 ).isActive = true
        selectedView.heightAnchor.constraint(equalToConstant: 5).isActive = true
        
    }
 
    
}
