//
//  CustomTitleButton.swift
//  bnetLayout01
//
//  Created by Denis Velikanov on 23.09.2021.
//

import Foundation
import UIKit

class CustomTitleButton: UIButton {
    
    open var buttonEnabled: Bool = false {
           didSet {
               alpha = buttonEnabled ? 1.0 : 0.5
            setTitleColor(.white, for: .normal)
            setTitleColor(.gray, for: .selected)
           }
       }
    
    init() {
        super.init(frame: .zero)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
