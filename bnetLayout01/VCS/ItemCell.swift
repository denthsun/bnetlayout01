//
//  ItemCell.swift
//  bnetLayout01
//
//  Created by Denis Velikanov on 22.09.2021.
//

import UIKit

class ItemCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
        constraintUI()
        contentView.backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static let identifier = "ItemCell"
    
    var mainLabel = UILabel()
    let moreButton = UIButton()
    
    let middleStack = UIView(frame: CGRect(x: 0, y: 0, width: 290, height: 50))
    var subThemeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
    let blockStack = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 20))
    let blockCountLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 60, height: 18))
    var blocksGreenLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 23, height: 18))
    let blockRedLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 23, height: 18))
    let checkCircle = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
    let fullLine = UIView()
    let redLine = UIView(frame: CGRect(x: 0, y: 0, width: 17, height: 4))
    
    let lowStack = UIView(frame: CGRect(x: 0, y: 0, width: 290, height: 40))
    var estimateLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 211, height: 19))
    let thumbUp = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
    let thumbDown = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))

    func setupUI() {
        [mainLabel, moreButton,
         middleStack,
         lowStack].forEach { contentView.addSubview($0) }
        
        [subThemeLabel, blockStack, checkCircle, fullLine].forEach { middleStack.addSubview($0) }
        [redLine].forEach { fullLine.addSubview($0) }
        [blockRedLabel, blocksGreenLabel, blockCountLabel].forEach { blockStack.addSubview($0) }
        [estimateLabel, thumbUp, thumbDown].forEach { lowStack.addSubview($0) }
        
        contentView.enableCornerRadius(radius: 8)
//        middleStack.backgroundColor = .orange
//        lowStack.backgroundColor = .systemIndigo
//
        mainLabel.textColor = .black
        mainLabel.font = UIFont.boldSystemFont(ofSize: 16)
        mainLabel.numberOfLines = 0
        mainLabel.lineBreakMode = .byWordWrapping
        
        blockCountLabel.textColor = .gray
        blockCountLabel.font = UIFont.systemFont(ofSize: 14)
        blockCountLabel.adjustsFontSizeToFitWidth = true
        
        [blockRedLabel, blocksGreenLabel].forEach { $0.font = UIFont.systemFont(ofSize: 14) }
        [blockRedLabel, blocksGreenLabel].forEach { $0.adjustsFontSizeToFitWidth = true }
        blockRedLabel.textColor = .red
        blocksGreenLabel.textColor = .green
        
        subThemeLabel.textColor = .gray
        subThemeLabel.font = UIFont.systemFont(ofSize: 14)
        
        let thumbUpImage = UIImage(named: "thumbs-up.png")
        let thumbDownImage = UIImage(named: "thumbs-down.png")
        let moreImage = UIImage(named: "More.png")
        let checkImage = UIImage(named: "outline-check_circle.png")
        
        thumbUp.setImage(thumbUpImage, for: .normal)
        thumbDown.setImage(thumbDownImage, for: .normal)
        checkCircle.image = checkImage
        moreButton.setImage(moreImage, for: .normal)
        
        thumbUp.addTarget(self, action: #selector(thUPTap), for: .touchUpInside)
        thumbDown.addTarget(self, action: #selector(thDWTap), for: .touchUpInside)
        
        estimateLabel.textColor = .gray
        estimateLabel.font = UIFont.systemFont(ofSize: 14)
        estimateLabel.text = "Оценить материал"
    
        [fullLine, redLine].forEach { $0.layer.cornerRadius = 4 }

        fullLine.backgroundColor = .gray
        redLine.backgroundColor = .red
        
    }
    
    func constraintUI() {
        
        mainLabel.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                         leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                         bottom: middleStack.safeAreaLayoutGuide.topAnchor,
                         trailing: moreButton.safeAreaLayoutGuide.leadingAnchor,
                         padding: .init(top: 20, left: 16, bottom: 6, right: 0))
        
        mainLabel.heightAnchor.constraint(equalToConstant: 65).isActive = true
        mainLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20).isActive = true
        
        moreButton.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                          leading: mainLabel.safeAreaLayoutGuide.trailingAnchor,
                          bottom: nil,
                          trailing: contentView.safeAreaLayoutGuide.trailingAnchor,
                          padding: .init(top: 18, left: 0, bottom: 0, right: 8))
        
        moreButton.widthAnchor.constraint(equalToConstant: 24).isActive = true
        moreButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
        moreButton.leadingAnchor.constraint(equalTo: mainLabel.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        moreButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 18).isActive = true
        
        
        middleStack.anchor(top: mainLabel.safeAreaLayoutGuide.bottomAnchor,
                               leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                               bottom: lowStack.safeAreaLayoutGuide.topAnchor,
                               trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
        middleStack.heightAnchor.constraint(equalToConstant: 60).isActive = true
        [subThemeLabel, blockStack, checkCircle].forEach { $0.centerYAnchor.constraint(equalTo: middleStack.centerYAnchor).isActive = true }
        subThemeLabel.heightAnchor.constraint(equalTo: blockStack.heightAnchor).isActive = true
        
        subThemeLabel.anchor(top: middleStack.safeAreaLayoutGuide.topAnchor,
                             leading: middleStack.safeAreaLayoutGuide.leadingAnchor,
                             bottom: middleStack.safeAreaLayoutGuide.bottomAnchor,
                             trailing: blockStack.safeAreaLayoutGuide.leadingAnchor,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 25))
        
        blockStack.anchor(top: middleStack.safeAreaLayoutGuide.topAnchor,
                           leading: subThemeLabel.safeAreaLayoutGuide.trailingAnchor,
                           bottom: fullLine.safeAreaLayoutGuide.topAnchor,
                           trailing: nil,
                           padding: .init(top: 3, left: 30, bottom: 0, right: 0))

        
        blockRedLabel.anchor(top: blockStack.safeAreaLayoutGuide.topAnchor,
                             leading: blockStack.safeAreaLayoutGuide.leadingAnchor,
                             bottom: blockStack.safeAreaLayoutGuide.bottomAnchor,
                             trailing: blockCountLabel.safeAreaLayoutGuide.leadingAnchor,
                             padding: .init(top: 0, left: 0, bottom: -4, right: 0))
        
        blocksGreenLabel.anchor(top: blockStack.safeAreaLayoutGuide.topAnchor,
                                leading: blockStack.safeAreaLayoutGuide.leadingAnchor,
                                bottom: blockStack.safeAreaLayoutGuide.bottomAnchor,
                                trailing: blockCountLabel.safeAreaLayoutGuide.leadingAnchor)
        
        blockCountLabel.anchor(top: blockStack.safeAreaLayoutGuide.topAnchor,
                               leading: blocksGreenLabel.safeAreaLayoutGuide.trailingAnchor,
                               bottom: blockStack.safeAreaLayoutGuide.bottomAnchor,
                               trailing: blockStack.safeAreaLayoutGuide.trailingAnchor)
        
        checkCircle.anchor(top: nil,
                           leading: nil, bottom: nil,
                           trailing: middleStack.safeAreaLayoutGuide.trailingAnchor,
                           padding: .init(top: 0, left: 0, bottom: 0, right: 12))
        
        checkCircle.widthAnchor.constraint(equalToConstant: 24).isActive = true
        checkCircle.heightAnchor.constraint(equalToConstant: 24).isActive = true


        lowStack.anchor(top: middleStack.safeAreaLayoutGuide.bottomAnchor,
                        leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                        bottom: contentView.safeAreaLayoutGuide.bottomAnchor,
                        trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
        lowStack.heightAnchor.constraint(equalToConstant: 50).isActive = true
        [estimateLabel, thumbUp, thumbDown].forEach { $0.centerYAnchor.constraint(equalTo: lowStack.centerYAnchor).isActive = true }
        
        
        estimateLabel.anchor(top: lowStack.safeAreaLayoutGuide.topAnchor,
                             leading: lowStack.safeAreaLayoutGuide.leadingAnchor,
                             bottom: lowStack.safeAreaLayoutGuide.bottomAnchor,
                             trailing: thumbUp.safeAreaLayoutGuide.leadingAnchor,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 0))
    
        thumbUp.anchor(top: lowStack.safeAreaLayoutGuide.topAnchor,
                       leading: estimateLabel.safeAreaLayoutGuide.trailingAnchor,
                       bottom: lowStack.safeAreaLayoutGuide.bottomAnchor,
                       trailing: thumbDown.safeAreaLayoutGuide.leadingAnchor,
                       padding: .init(top: 00, left: 10, bottom: 0, right: 12))
        thumbUp.widthAnchor.constraint(equalToConstant: 24).isActive = true
        thumbUp.heightAnchor.constraint(equalToConstant: 24).isActive = true
    
        thumbDown.anchor(top: lowStack.safeAreaLayoutGuide.topAnchor,
                         leading: thumbUp.safeAreaLayoutGuide.trailingAnchor,
                         bottom: lowStack.safeAreaLayoutGuide.bottomAnchor,
                         trailing: lowStack.safeAreaLayoutGuide.trailingAnchor,
                         padding: .init(top: 5, left: 12, bottom: 0, right: 9))
        thumbDown.widthAnchor.constraint(equalToConstant: 24).isActive = true
        thumbDown.heightAnchor.constraint(equalToConstant: 24).isActive = true

        redLine.anchor(top: fullLine.safeAreaLayoutGuide.topAnchor,
                       leading: fullLine.safeAreaLayoutGuide.leadingAnchor,
                       bottom: fullLine.safeAreaLayoutGuide.bottomAnchor,
                       trailing: fullLine.safeAreaLayoutGuide.trailingAnchor,
                       padding: .init(top: -8, left: 0, bottom: 0, right: 200))

        fullLine.anchor(top: blockStack.safeAreaLayoutGuide.bottomAnchor,
                        leading: subThemeLabel.safeAreaLayoutGuide.trailingAnchor,
                        bottom: middleStack.safeAreaLayoutGuide.bottomAnchor,
                        trailing: middleStack.safeAreaLayoutGuide.trailingAnchor,
                        padding: .init(top: 0, left: 30, bottom: 0, right: 20))
        redLine.centerYAnchor.constraint(equalTo: fullLine.centerYAnchor).isActive = true
        fullLine.heightAnchor.constraint(equalToConstant: 4).isActive = true
        redLine.heightAnchor.constraint(equalToConstant: 4).isActive = true

    }
    
    @objc func thUPTap() {
        print("UP")

    
    }
    @objc func thDWTap() {
        print("Down")

    }
    
}

