//
//  SplashViewController.swift
//  bnetLayout01
//
//  Created by Denis Velikanov on 22.09.2021.
//

import UIKit

class SplashViewController: UIViewController {
  
    let topTitleView = UIView()
    let backtitleImage = UIImageView()
    let titleButtonsStack = UIView()
    let navCupButton = UIButton()
    let backButton = UIButton()
    let titleLabel = UILabel()
    
    let titleLowStack = UIStackView()
    let layer01Button = CustomTitleView(frame: .zero,
                                  isEnabled: false,
                                  titleText: "Уровень 1")
    let layer02Button = CustomTitleView(frame: .zero,
                                  isEnabled: true,
                                  titleText: "Уровень 2")
    let layer03button = CustomTitleView(frame: .zero,
                                  isEnabled: false,
                                  titleText: "Уровень 3")
    
    let alertView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 64))
    let alertCloseButton = UIButton()
    let alertGoButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 16))
    let alertGoLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 16))
    let alertLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 174, height: 48))
    
    
    let dataProvider = DataProvider()
    
    let collectionView = UICollectionView(frame: .init(),
                                          collectionViewLayout: UICollectionViewFlowLayout.init())
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()
        constraintUI()
        setupCollectionView()
        setLayout()
    }

    func setLayout() {
        let screenWidth = view.layer.bounds.width - 26
        layout.scrollDirection = .vertical
        layout.itemSize = .init(width: screenWidth, height: 190)
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)
        layout.minimumLineSpacing = 15
        
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    

    
    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        
        collectionView.register(ItemCell.self, forCellWithReuseIdentifier: ItemCell.identifier)
        
    }
    
    
    func setupUI() {
        [topTitleView, collectionView, alertView].forEach { view.addSubview($0) }
        topTitleView.addSubview(backtitleImage)
        [titleButtonsStack, titleLabel, titleLowStack].forEach { backtitleImage.addSubview($0) }
        [backButton, navCupButton].forEach { titleButtonsStack.addSubview($0) }
        
        [layer01Button, layer02Button, layer03button].forEach { titleLowStack.addArrangedSubview($0) }
        titleLowStack.axis = .horizontal
        titleLowStack.distribution = .fillEqually
        titleLowStack.isUserInteractionEnabled = true
        
        layer01Button.button.addTarget(self, action: #selector(button1Tapped), for: .touchUpInside)
        layer02Button.button.addTarget(self, action: #selector(button2Tapped), for: .touchUpInside)
        layer03button.button.addTarget(self, action: #selector(button3Tapped), for: .touchUpInside)
        
        
        [alertCloseButton, alertLabel, alertGoButton, alertGoLabel].forEach { alertView.addSubview($0) }
        
        
        titleLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 19)
        titleLabel.text = "Системы утепления фасадов"
        
        let backImage = UIImage(named: "Back.png")
        backButton.setImage(backImage, for: .normal)
        backButton.addTarget(self, action: #selector(backTapped), for: UIControl.Event.touchUpInside)
        
        let cupImage = UIImage(named: "nav_cup.png")
        navCupButton.setImage(cupImage, for: .normal)
        navCupButton.addTarget(self, action: #selector(cupTapped), for: UIControl.Event.touchUpInside)

        backtitleImage.image = UIImage(named: "DSC_0775.png")
        backtitleImage.contentMode = .scaleToFill
        backtitleImage.isUserInteractionEnabled = true
        
        alertView.backgroundColor = .gray
        let closeImage = UIImage(named: "close_black_large.png")
        alertCloseButton.setImage(closeImage, for: .normal)
        
        alertLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        alertLabel.font = UIFont.systemFont(ofSize: 14)
        alertLabel.numberOfLines = 0
        alertLabel.lineBreakMode = .byWordWrapping
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 0.94
        alertLabel.attributedText = NSMutableAttributedString(string: "На этом уровне можно \nсразу перейти итоговому тестированию", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        
        alertGoLabel.textColor = UIColor(red: 0.89, green: 0.024, blue: 0.075, alpha: 1)
        alertGoLabel.font = UIFont.systemFont(ofSize: 14)
        let paragraphStyle1 = NSMutableParagraphStyle()
        paragraphStyle1.lineHeightMultiple = 0.94
        alertGoLabel.textAlignment = .right
        alertGoLabel.attributedText = NSMutableAttributedString(string: "   Пройти", attributes: [NSAttributedString.Key.kern: 0.75, NSAttributedString.Key.paragraphStyle: paragraphStyle1])
        
        alertCloseButton.addTarget(self, action: #selector(alertCancelTapped), for: .touchUpInside)
    }
    
    func constraintUI() {
        
        topTitleView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                          leading: view.safeAreaLayoutGuide.leadingAnchor,
                          bottom: collectionView.safeAreaLayoutGuide.topAnchor,
                          trailing: view.safeAreaLayoutGuide.trailingAnchor)
        topTitleView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        backtitleImage.anchor(top: topTitleView.safeAreaLayoutGuide.topAnchor,
                              leading: topTitleView.safeAreaLayoutGuide.leadingAnchor,
                              bottom: topTitleView.safeAreaLayoutGuide.bottomAnchor,
                              trailing: topTitleView.safeAreaLayoutGuide.trailingAnchor)
        
        titleButtonsStack.anchor(top: backtitleImage.safeAreaLayoutGuide.topAnchor,
                                 leading: backtitleImage.safeAreaLayoutGuide.leadingAnchor,
                                 bottom: titleLabel.safeAreaLayoutGuide.topAnchor,
                                 trailing: backtitleImage.safeAreaLayoutGuide.trailingAnchor,
                                 padding: .init(top: 20, left: 10, bottom: 10, right: 20))
        titleButtonsStack.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        backButton.anchor(top: titleButtonsStack.safeAreaLayoutGuide.topAnchor,
                          leading: titleButtonsStack.safeAreaLayoutGuide.leadingAnchor,
                          bottom: titleButtonsStack.safeAreaLayoutGuide.bottomAnchor,
                          trailing: nil)
        
        navCupButton.anchor(top: titleButtonsStack.safeAreaLayoutGuide.topAnchor,
                            leading: nil,
                            bottom: titleButtonsStack.safeAreaLayoutGuide.bottomAnchor,
                            trailing: titleButtonsStack.safeAreaLayoutGuide.trailingAnchor)
        
        titleLabel.anchor(top: titleButtonsStack.safeAreaLayoutGuide.bottomAnchor,
                          leading: backtitleImage.safeAreaLayoutGuide.leadingAnchor,
                          bottom: titleLowStack.safeAreaLayoutGuide.topAnchor,
                          trailing: backtitleImage.safeAreaLayoutGuide.trailingAnchor,
                          padding: .init(top: -10, left: 20, bottom: 10, right: 10))
        
        titleLowStack.anchor(top: titleLabel.safeAreaLayoutGuide.bottomAnchor,
                             leading: backtitleImage.safeAreaLayoutGuide.leadingAnchor,
                             bottom: backtitleImage.safeAreaLayoutGuide.bottomAnchor,
                             trailing: backtitleImage.safeAreaLayoutGuide.trailingAnchor)
        titleLowStack.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        collectionView.anchor(top: topTitleView.safeAreaLayoutGuide.bottomAnchor,
                              leading: view.safeAreaLayoutGuide.leadingAnchor,
                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
                              trailing: view.safeAreaLayoutGuide.trailingAnchor)
        
        alertView.anchor(top: nil, leading: view.safeAreaLayoutGuide.leadingAnchor,
                         bottom: view.safeAreaLayoutGuide.bottomAnchor,
                         trailing: view.safeAreaLayoutGuide.trailingAnchor)
        alertView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        
        alertCloseButton.anchor(top: alertView.safeAreaLayoutGuide.topAnchor,
                                leading: alertView.safeAreaLayoutGuide.leadingAnchor,
                                bottom: alertView.safeAreaLayoutGuide.bottomAnchor,
                                trailing: alertLabel.safeAreaLayoutGuide.leadingAnchor)
        alertLabel.anchor(top: alertView.safeAreaLayoutGuide.topAnchor,
                          leading: alertCloseButton.safeAreaLayoutGuide.trailingAnchor,
                          bottom: alertView.safeAreaLayoutGuide.bottomAnchor,
                          trailing: alertGoLabel.safeAreaLayoutGuide.leadingAnchor)
        
        alertLabel.widthAnchor.constraint(equalToConstant: 174).isActive = true
        alertLabel.heightAnchor.constraint(equalToConstant: 48).isActive = true
        alertLabel.centerXAnchor.constraint(equalTo: alertView.centerXAnchor, constant: -21).isActive = true
        alertLabel.bottomAnchor.constraint(equalTo: alertView.bottomAnchor, constant: -9).isActive = true
        
        
        alertGoButton.anchor(top: alertView.safeAreaLayoutGuide.topAnchor,
                             leading: nil,
                             bottom: alertView.safeAreaLayoutGuide.bottomAnchor,
                             trailing: alertView.safeAreaLayoutGuide.trailingAnchor)
        
        
        alertGoLabel.anchor(top: alertView.safeAreaLayoutGuide.topAnchor,
                            leading: nil,
                            bottom: alertView.safeAreaLayoutGuide.bottomAnchor,
                            trailing: alertView.safeAreaLayoutGuide.trailingAnchor)
        
    }
    
    @objc func backTapped() {
        print("BACK")
        
        let alert = UIAlertController(title: "tap", message: "taap tap", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: {action in
            print("tap Dismiss")
        }))
        alert.addAction(UIAlertAction(title: "No hold on", style: .destructive, handler: {  action in
        }))
        
        present(alert, animated: true)
    }
    
    @objc func cupTapped() {
        print("CUP")
        
            let alert = UIAlertController(title: "tap", message: "taap tap", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: {action in
                print("tap Dismiss")
            }))
            alert.addAction(UIAlertAction(title: "No hold on", style: .destructive, handler: {  action in
            }))
            
            present(alert, animated: true)
        
        
    }
    
    @objc func buttonTapped() {
        let alert = UIAlertController(title: "tap", message: "taap tap", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: {action in
            print("tap Dismiss")
        }))
        alert.addAction(UIAlertAction(title: "No hold on", style: .destructive, handler: {  action in
        }))
        
        present(alert, animated: true)
    
    }
    
    @objc func alertCancelTapped() {
        alertView.isHidden = true
        let alert = UIAlertController(title: "tap", message: "taap tap", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: {action in
            print("tap Dismiss")
        }))
        alert.addAction(UIAlertAction(title: "No hold on", style: .destructive, handler: {  action in
        }))
        
        present(alert, animated: true)
    }
    
    
    @objc func button1Tapped() {
        let alert = UIAlertController(title: "tap", message: "taap tap", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: {action in
            print("tap Dismiss")
        }))
        alert.addAction(UIAlertAction(title: "No hold on", style: .destructive, handler: {  action in
        }))
        
        present(alert, animated: true)
    
        DispatchQueue.main.async {
            if self.layer01Button.buttonEnabled == false {
                self.layer01Button.buttonEnabled = true
                self.layer02Button.buttonEnabled = false
                self.layer03button.buttonEnabled = false
            }
        }
        
        
        print("check 1 check")
    }
    
    @objc func button2Tapped() {
        let alert = UIAlertController(title: "tap", message: "taap tap", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: {action in
            print("tap Dismiss")
        }))
        alert.addAction(UIAlertAction(title: "No hold on", style: .destructive, handler: {  action in
        }))
        
        present(alert, animated: true)
    
        DispatchQueue.main.async {
            if self.layer02Button.buttonEnabled == false {
                self.layer01Button.buttonEnabled = false
                self.layer02Button.buttonEnabled = true
                self.layer03button.buttonEnabled = false
            }
            print("check 2 check")
        }
    }
    
    @objc func button3Tapped() {
        let alert = UIAlertController(title: "tap", message: "taap tap", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: {action in
            print("tap Dismiss")
        }))
        alert.addAction(UIAlertAction(title: "No hold on", style: .destructive, handler: {  action in
        }))
        
        present(alert, animated: true)
    
        DispatchQueue.main.async {
            if self.layer03button.buttonEnabled == false {
                self.layer01Button.buttonEnabled = false
                self.layer02Button.buttonEnabled = false
                self.layer03button.buttonEnabled = true
            }
            print("check 3 check")
        }
    }
    
    
}

extension SplashViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataProvider.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCell.identifier, for: indexPath) as? ItemCell else { return UICollectionViewCell() }
        cell.mainLabel.text = dataProvider.items[indexPath.row].title
        cell.subThemeLabel.text = "\(dataProvider.items[indexPath.row].sub_themes) подтем"
        cell.blockRedLabel.text = "\(dataProvider.items[indexPath.row].blocks)"
        cell.blocksGreenLabel.text = "\(dataProvider.items[indexPath.row].blocks)"
        cell.blockCountLabel.text = "/\(dataProvider.items[indexPath.row].blocks_count) блоков"
        
        
        if dataProvider.items[indexPath.row].blocks == 96 {
            cell.blockRedLabel.isHidden = true
            cell.redLine.isHidden = true
            cell.fullLine.isHidden = true
            
        } else {
            cell.estimateLabel.isHidden = true
            cell.thumbUp.isHidden = true
            cell.thumbDown.isHidden = true
            cell.checkCircle.isHidden = true
            
        }
        
        cell.layer.cornerRadius = 15.0
        cell.layer.borderWidth = 0.0
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 1, height: 1)
        cell.layer.shadowRadius = 5.0
        cell.layer.shadowOpacity = 1
        cell.layer.masksToBounds = false
        
        return cell
    }
    
    
    
    
}






