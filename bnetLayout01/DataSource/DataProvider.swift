//
//  DataProvider.swift
//  bnetLayout01
//
//  Created by Denis Velikanov on 22.09.2021.
//

import Foundation

class DataProvider {
    let items = [
        ItemModel(title: "Материалы ТЕХНОНИКОЛЬ, применяемые для фасадных систем утепления",
                  test: 0, sub_themes: 10, blocks: 96, blocks_count: 96, status: "open"),
        ItemModel(title: "Материалы ТЕХНОНИКОЛЬ, применяемые для фасадных систем утепления", test: 0, sub_themes: 15, blocks: 50, blocks_count: 96, status: "open"),
        ItemModel(title: "Материалы ТЕХНОНИКОЛЬ, применяемые для фасадных систем утепления", test: 0, sub_themes: 20, blocks: 0, blocks_count: 96, status: "closed")
    ]
    
}
